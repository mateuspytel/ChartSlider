import { Component, OnInit, HostListener, EventEmitter, ElementRef, ViewChild, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/takeUntil';
import { EventManager } from '@angular/platform-browser';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  @ViewChild('leftRec') leftRec: ElementRef;
  @ViewChild('rightRec') rightRec: ElementRef;
  @ViewChild('chartarea') chartArea: ElementRef;

  _chartData = {
    options : {
      'title' : 'Sample Chart',
      'height' : 400,
    },
    chartType: 'LineChart',
    dataTable: [
      ['Task', 'Hours per day'],
      ['Work', 16],
      ['Eating', 2],
      ['Playing games', 1],
      ['Reading', 5],
      ['Sleeping', 5]
    ]
  };

  _previewData = {
    options : {
      height : 200,
      legend: 'none',
      chartArea:{top: -20,width:"98%",height:"90%"}
    },
    chartType: 'AreaChart',
    dataTable: [
      ['Task', 'Hours per day'],
      ['Work', 16],
      ['Eating', 2],
      ['Playing games', 1],
      ['Reading', 5],
      ['Sleeping', 5]
    ]
  };

  sliderActive = false;

  sliders = { 
    left : false,
    right : false,
    leftPos: 0,
    rightPos: 100
  };

  activateSlider(index: number) {
    switch(index) {
      case 1: {
        this.sliders.left = true; 
        break;
      }
      case 2: {
        this.sliders.right = true;
        break;
      }
    }
  }

  dragSlider(event: MouseEvent) { 
    if (this.sliders.left) {
      this.calculateLeftSlider(event);
    } // If right slider was grabbed
    else if (this.sliders.right) {
      this.calculateRightSlider(event);
    }
  }

  private calculateLeftSlider(event: MouseEvent): void {

    // Get draggable container bounding rect
    let sliderRect = this.chartArea.nativeElement.getBoundingClientRect();

    // Calculate relative position of slider according to page
    let relPos = event.pageX - sliderRect.left;

    // Calculate relativeStep [%]
    let relativeStep = (relPos / (sliderRect.right - sliderRect.left));

    // Check if mouse position is within a draggable rect 
    let mouseRelationToRect = this.checkPointRelation(event.pageX, sliderRect);
    
    // Handle all possible sceneraios of mousePosition relation to slider rect
    switch(mouseRelationToRect) {
      case -1: {                                                                      // -1 : Mouse is outside from left side of the rect
        this.leftRec.nativeElement.style.width = 0; //minLeft + 'px';
        break;
      }
      case 0: {                                                                       // 0 : Mouse is inside rect

        // If left slider position if lower then right slider move it freely
        // if not lock it's position equally to the right slider  
        if (event.pageX <= this.rightRec.nativeElement.getBoundingClientRect().left) {
          this.leftRec.nativeElement.style.width = relPos + 'px';
          // console.log('Step: ' + relativeStep);
        } else {
          this.leftRec.nativeElement.style.width = this.rightRec.nativeElement.getBoundingClientRect().left - sliderRect.left + 'px';
        }
        break;
      }
    }
  }

  private calculateRightSlider(event: MouseEvent): void {

    // Get draggable container bounding rect
    let sliderRect = this.chartArea.nativeElement.getBoundingClientRect();
    
    // Calculate relative width of slider container according to page
    let relWidth = sliderRect.right -  event.pageX;

    // Calculate relativeStep [%]
    let relativeStep = (1 - (relWidth / (sliderRect.right - sliderRect.left)));

    // Check if mouse position is within a draggable rect 
    let mouseRelationToRect = this.checkPointRelation(event.pageX, sliderRect);

    // Handle all possible sceneraios of mousePosition relation to slider rect
    switch(mouseRelationToRect) {
      case 0: {                                                                       // 0 : Mouse is inside rect

        // If left slider position if lower then right slider move it freely
        // if not lock it's position equally to the right slider  
        if (event.pageX >= this.leftRec.nativeElement.getBoundingClientRect().right) {
          this.rightRec.nativeElement.style.width = relWidth + 'px';
          console.log('Relative step: ' + relativeStep);
          this.cropDataArray(this._chartData.dataTable, 0, relativeStep);
        } else {
          this.rightRec.nativeElement.style.width = sliderRect.right - this.leftRec.nativeElement.getBoundingClientRect().right + 'px';
        }
        break;
      }

      case 1: {
        this.rightRec.nativeElement.style.width = 0;
        break;
      }
    }
  }

  private cropDataArray(arr: any, leftstep: number, rightstep: number) {
    let rightIndex = 0;
    if (rightstep) {
      rightIndex = Math.floor(rightstep * arr.length);
    }
    let reduced = arr.slice(0, rightIndex + 1);
    console.log('Reduced arr: ' + JSON.stringify(reduced, null, 4));
  }

  private checkPointRelation(point: number, rect: {left, right}): number {
    return (point < rect.left) ? -1 : ((point < rect.right) ? 0 : 1);
  }

  deactivateSliders() {
    this.sliders.left = false; 
    this.sliders.right = false;
  }

  constructor(public element : ElementRef) { }

  ngOnInit() {}

}
