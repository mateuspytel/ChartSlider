import { Component, OnInit } from '@angular/core';
import { trigger, transition, query, style, stagger, animate } from '@angular/animations';
import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/mergeMap';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('rowAnimation', [
      transition('* => *', [
        query('.contentrow', style({ transform: 'translateY(-60%)' }), { limit: 15 }),
        query('.contentrow', stagger('30ms', [
          animate('150ms', style({ transform: 'translateY(0)' }))
        ]), { limit: 15 })
      ])
    ])
  ]
})
export class AppComponent implements OnInit {
  title = 'app';
  _sampleObject = {
    id: 213,
    name : 'Michael',
    lastname : 'Kiwanuka',
    date: new Date().getTime()
  };

  _props: Array<string>;
  _data : any;

  _dictionary = {
    id: 'ID',
    name: 'First Name',
    lastname: 'Last Name',
    email: 'Email address',
    address: 'Address',
    identifier: 'Personal ID',
    date: 'Creation date'
  }

  _sticky = ['identifier'];
  _nonsticky: Array<string>;
  _stickyContainerWidth: string;
  _ready = false;
  _sortOptions = {};
  STICKYCOLUMNWIDTH = 80;

  private store = new Subject();
  private __data: any;

  public keyUp = new Subject<string>();
  constructor() {}

  sortByProperty(property: string) {
    this._ready = !this._ready;
    let order = this._sortOptions[property] ? (this._sortOptions[property] === 1 ? -1 : 1) : 1;
    this._sortOptions = {};
    this._sortOptions[property] = order;
    this.store.next( this._data.sort(this.compareByPropertyName(property, order)));
  }

  private compareByPropertyName(property: string, order: number) {
    return function (a,b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * order;
    }
  }

  private generateRandomData(quantuty: number) {
    const names = ['Adam', 'John', 'Arnold', 'Michael', 'Jack', 'Jerry', 'Rick', 'Morty'];
    const emails = ['mail@mail.pl', 'mail2@gmail.com', 'bpytel@interia.pl', 'rick@morty.com'];
    const LOCATIONS = [
      '421 E DRACHMAN TUCSON AZ 85705-7598 USA', 
      'SUITE 5A-1204 799 E DRAGRAM TUCSON AZ 85705 USA', 
      'POB 65502 TUCSON AZ 85728 USA'
    ];
    const output = [];
    for (let i=0; i<quantuty; i++) {
      const name = names[Math.floor(Math.random() * names.length)]
      let obj = {
        id: i,
        name: name,
        lastname: name + " " + i,
        email: emails[Math.floor(Math.random() * emails.length)],
        address: LOCATIONS[Math.floor(Math.random() * LOCATIONS.length)],
        identifier: Math.floor(Math.random() * 100000),
        date: new Date().toLocaleDateString()
      }
      output.push(obj);
    }

    return output;
  }

  filterArray(phrase: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (phrase === 'reset') resolve(this.__data);

      let result = [];
      this._data.forEach( element => {
        for (let key in element) {
          if (typeof element[key] === 'string' && element[key].indexOf(phrase) > -1) {
            result.push(element);
          }
        }
      });

      if (result.length > 0 ) resolve(result);
      else reject('No results');
    });
  }

  ngOnInit() {

    this.store.asObservable().subscribe( data => {
      this._data = data;
    });

    this.store.next(this.generateRandomData(255));

    this.__data = this._data;
    this._stickyContainerWidth = (this.STICKYCOLUMNWIDTH * this._sticky.length) + 'px';
    this._props = Object.keys(this._data[0]);
    this._nonsticky = this._props.filter( prop => this._sticky.indexOf(prop) === -1 );

    const subscription = this.keyUp.map( (event: any) => event.target.value)
    .distinctUntilChanged()
    .flatMap( search => Observable.of(search).delay(500))
    .filter( search => search.length > 3)
    .subscribe( phrase => {
      this.filterArray(phrase)
      .then( result => {
        console.log('Found: ' + result.length + ' records');
        this.store.next(result);
        this._ready = !this._ready;
      })
      .catch( err => {
        console.error(err);
      });
    });
  }


}
