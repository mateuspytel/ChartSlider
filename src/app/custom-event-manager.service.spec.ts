import { TestBed, inject } from '@angular/core/testing';

import { CustomEventManagerService } from './custom-event-manager.service';

describe('CustomEventManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomEventManagerService]
    });
  });

  it('should be created', inject([CustomEventManagerService], (service: CustomEventManagerService) => {
    expect(service).toBeTruthy();
  }));
});
